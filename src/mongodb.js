const { MongoClient } = require('mongodb')
const connectionURI = 'mongodb://127.0.0.1:27017'
const databaseName = 'quotes'

const client = new MongoClient(connectionURI);
async function run() {
    try {
        await client.connect()
        await client.db("admin").command({ ping:1});
        console.log("Connected Successfully to server")
    } finally {
        await client.close();
    }
}

run().catch(console.dir)



async function create_quote(quote, author) {
    const db = client.db(databaseName)
    db.collection('quotes').insertOne({
        quote: quote,
        author: author
    }, (error, result) => {
        if (error) {
            console.log(error)
            return console.log("Unable to create quote")
        }
    }
    )
}

async function get_by_id(id) {
        db.collection('quotes').findOne({id: id}, (error, quote)=>{
                    if (error) {
                        return console.log('Unable to fetch')
                    }
            
                    console.log(quote)
                })

    }


async function search(text) {
    await client.connect()
    const db = client.db(databaseName)
    db.collection('quotes').findOne(
        { $text: {$search:text}}, 
        (error, quote)=>{
                if (error) {
                    return console.log('Unable to fetch')
                }
        
                console.log(quote)
            })

}

module.exports = { create_quote, get_by_id}