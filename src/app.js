const express = require('express')
const path = require('path')
const hbs = require('hbs')
const req = require('express/lib/request')
// console.log(new quote(1,'asd','asd')) this works

const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')
//const { create_quote, get_by_id } = require('./mongodb')


const { MongoClient, ObjectId } = require('mongodb')
const connectionURI = 'mongodb://127.0.0.1:27017'
const databaseName = 'AllQuotes'

const client = new MongoClient(connectionURI);
async function run() {
    try {
        await client.connect()
        client.db(databaseName)
        //.command({ ping:1})
        console.log("Connected Successfully to server")
    } finally {
        await client.close();
    }
}

run().catch(console.dir)


async function create_quote(quote, author) {
    await client.connect()
    const db = client.db(databaseName)
    db.collection('quotes').insertOne({
        quote: quote,
        author: author
    }, (error, result) => {
        if (error) {
            console.log(error)
            return console.log("Unable to create quote")
        }
    })
}

async function get_by_id(id) {
    await client.connect()
    const db = client.db(databaseName)
    db.collection('quotes').findOne({
        _id: new ObjectId(id)}, 
        (error, quote)=>{
                if (error) {
                    return console.log('Unable to fetch')
                }
        
                console.log(quote)
            })

}

async function search_quote(text) {
    await client.connect()
    const col = client.db(databaseName).collection('quotes')

    const query = {quote: { $regex: text} }
    const cursor = col.find(query)
    const results = await cursor.toArray()

    console.log(results)
}


async function get_all_quotes() {
    await client.connect()
    const col = client.db(databaseName).collection('quotes')

    col.find({}).toArray( (error, result) => {
        if (error) throw error;
        console.log(result)
    })
}

async function update_quote(id, quote, author){
    await client.connect()
    const col = client.db(databaseName).collection('quotes')

    const query = {$set: {quote: quote}};//, author: author}};

    col.updateOne({_id: new ObjectId(id)}, query, (error, result) => {
        if (error) throw error;
        console.log(result)
    })
}

async function delete_quote(id) {
    await client.connect()
    const col = client.db(databaseName).collection('quotes')

    col.deleteOne({_id: new ObjectId(id)}, (error, quote)=>{
        if (error) {
            console.log(error)
            return console.log("Unable to delete quote")
        }
        console.log(quote)
        }
    )
}

async function random_quote(){
    await client.connect()
    const col = client.db(databaseName).collection('quotes')
    const results = await col.aggregate([{$sample: {size: 1}}]).toArray()
    //.forEach(e=>{console.log(e)})
    console.log(results)
}

app = express()

app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)
app.use(express.static(publicDirectoryPath))

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res)=>{
    res.render('index', {})
})

app.post('/create_form', (req,res)=>{
    console.log(req.body)
    create_quote(req.body.Quote, req.body.Author)
    res.redirect('/')
//    res.render('index', {})
})

app.get('/update', (req, res)=>{
    res.render('update', {})
})

app.post('/update_form', (req, res)=>{
    console.log(req.body)
    update_quote(req.body.Id, req.body.Quote)
    res.redirect('update')
})

app.get('/delete', (req, res)=>{
    res.render('delete', {})
})

app.post('/delete_form', (req, res)=>{
    delete_quote(req.body.Id)
    res.redirect('delete')
})

app.get('/get', (req, res)=>{
    res.render('get', {})
})

app.post('/get_form', (req, res)=>{
    get_by_id(req.body.Id)
    res.redirect('get')
})

app.get('/random', (req, res)=>{
    res.render('random', {})
})

app.post('/random_quote_form', (req, res)=>{
    random_quote()
    res.redirect('/random')
})

app.get('/get_all', (req, res)=>{
    res.render('get_all', {})
})

app.post('/get_all_form', (req, res)=>{
    console.log(req.body)
    get_all_quotes()
    res.redirect('/get_all')
})

app.get('/search', (req, res)=>{
    res.render('search', {})
})

app.post('/search_form', (req, res)=>{
    console.log(req.body)
    search_quote(req.body.text)
    res.redirect('/search')
})

port=3005
app.listen(port,()=>{
    console.log("Initiate Successful")
})

